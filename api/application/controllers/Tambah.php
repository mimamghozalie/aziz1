<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tambah extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('qc');
	}

	public function index()
	{
		$data = $this->input->post();
		$tanggal = $data['tanggal'];
		$tgl = new DateTime($tanggal);
		$data['tanggal'] = $tgl->format('Y-m-d H:i:s');
		$query = $this->db->insert('tb_qc', $data);

		if ($query) {
			echo 'ok';
		} else {
			echo 'error';
		}
	}

	public function cari() {
		$data = $this->input->post();
		$tanggal = $data['tanggal'];
		$tgl = new DateTime($tanggal);
		$date = $tgl->format('Y-m-d H:i:s');
		$query = $this->db->select('*')->where('tanggal', $date)->from('tb_qc')->get();
		// $query = $this->db->get_where('tb_qc', array('tanggal', '$date'))->row_array();
		echo json_encode($query->result());
	}

	public function hapus() {
		
		$id = $this->input->post('id');
		$this->db->where('id_qc', $id);
		$query = $this->db->delete('tb_qc');

		if ($query) {
			echo 'ok';
		} else {
			echo 'error';
		}
	}
}
