<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Qc extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function tambahData($data) {
		$this->db->insert('tb_qc',$data);
	}

}
