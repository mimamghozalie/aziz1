-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22 Mar 2020 pada 04.43
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yuda1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_qc`
--

CREATE TABLE `tb_qc` (
  `id_qc` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `shift` varchar(255) NOT NULL,
  `kode_mesin` varchar(255) NOT NULL,
  `jam` varchar(255) NOT NULL,
  `panjang` varchar(255) NOT NULL,
  `tebal` varchar(255) NOT NULL,
  `print` varchar(11) NOT NULL,
  `emboss` varchar(11) NOT NULL,
  `panjang_cek` varchar(255) NOT NULL,
  `lipatan` varchar(11) NOT NULL,
  `kopel` varchar(11) NOT NULL,
  `profil` varchar(11) NOT NULL,
  `lurus` varchar(11) NOT NULL,
  `lapisan` varchar(11) NOT NULL,
  `qc_cek` enum('pass','reject','','') NOT NULL,
  `grade` varchar(255) NOT NULL,
  `operator` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_qc`
--

INSERT INTO `tb_qc` (`id_qc`, `tanggal`, `shift`, `kode_mesin`, `jam`, `panjang`, `tebal`, `print`, `emboss`, `panjang_cek`, `lipatan`, `kopel`, `profil`, `lurus`, `lapisan`, `qc_cek`, `grade`, `operator`) VALUES
(1, '0000-00-00 00:00:00', '1', '', '', '', '', '0', '0', '', '0', '0', '0', '0', '0', 'pass', '', ''),
(2, '0000-00-00 00:00:00', '1', '', '', '', '', '0', '0', '', '0', '0', '0', '0', '0', 'pass', '', ''),
(3, '0000-00-00 00:00:00', '1', '', '', '', '', '', '0', '', '0', '0', '0', '0', '0', 'pass', '', ''),
(4, '2020-03-13 00:00:00', '1', '', '', '', '', '', '0', '', '0', '0', '0', '0', '0', 'pass', '', ''),
(5, '2020-03-13 00:00:00', '1', '', '', '', '', '', '0', '', '0', '0', '0', '0', '0', 'pass', '', ''),
(6, '2020-03-12 00:00:00', '1', '', '', '', '', '', '0', '', '0', '0', '0', '0', '0', 'pass', '', ''),
(7, '2020-03-12 00:00:00', '1', '', '', '', '', '', '0', '', '0', '0', '0', '0', '0', 'pass', '', ''),
(8, '2020-03-28 00:00:00', '1', '', '', '', '', '', '0', '', '0', '0', '0', '0', '0', 'pass', '', ''),
(9, '2020-03-31 00:00:00', '1', '', '', '', '', '', '0', '', '0', '0', '0', '0', '0', 'pass', '', ''),
(10, '2020-03-16 00:00:00', '2', 'adas', '15:26', 'asd', 'sad', 'on', '0', '222', '0', '0', '0', '0', '0', 'pass', '', ''),
(11, '2020-03-16 00:00:00', '2', 'adas', '15:26', 'asd', 'sad', 'on', '0', '222', '0', '0', '0', '0', '0', 'pass', '', ''),
(12, '2020-03-18 00:00:00', '2', 'adas', '14:29', 'asd', 'dsadd', 'on', '0', 'sad', '0', '0', '0', '0', '0', 'pass', '', ''),
(13, '2020-03-20 00:00:00', '2', 'adas', '14:48', 'asd', 'asdsad', 'on', '0', '231223123', '0', '0', '0', '0', '0', 'pass', '', ''),
(14, '2020-03-20 00:00:00', '1', 'adas', '14:48', 'asd', 'asdsad', 'on', '0', '231223123', '0', '0', '0', '0', '0', 'pass', '', ''),
(15, '2020-03-20 00:00:00', '1', 'adas', '14:48', 'asd', 'asdsad', 'on', '0', '231223123', '0', '0', '0', '0', '0', 'pass', '', ''),
(16, '2020-03-20 00:00:00', '1', 'adas', '14:48', 'asd', 'asdsad', 'on', '0', '231223123', '0', '0', '0', '0', '0', 'pass', '', ''),
(17, '2020-03-20 00:00:00', '1', 'adas', '14:48', 'asd', 'asdsad', 'on', 'on', '231223123', 'on', 'on', 'on', 'on', 'on', 'pass', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_qc`
--
ALTER TABLE `tb_qc`
  ADD PRIMARY KEY (`id_qc`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_qc`
--
ALTER TABLE `tb_qc`
  MODIFY `id_qc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
